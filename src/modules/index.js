import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import appstate from './appstate'

export default combineReducers({
  routing: routerReducer,
  appstate
})