import axios from 'axios'

export const TOGGLE = 'course/TOGGLE'

export const GET_COURSE = 'course/GET_COURSE'
export const COURSE_RECEIVED = 'course/COURSE_RECEIVED'
export const GET_COURSE_ERROR = 'course/GET_COURSE_ERROR'

export const SET_CURRENT_PAGE = 'course/SET_CURRENT_PAGE'
export const SET_CURRENT_PAGE_DONE = 'course/SET_CURRENT_PAGE_DONE'

const courseUrl = 'http://localhost:4000/api/course/'

const initialState = {
  menuIsOpen: false,
  isLoading: true,
  course: null,
  nonExist: false,
  chapters: [],
  currentpage: null
}

export default (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE:
      return {
        ...state,
        menuIsOpen: !state.menuIsOpen
      }

    case GET_COURSE:
      return {
        ...state,
        isLoading: true
      }

    case COURSE_RECEIVED:
      return {
        ...state,
        isLoading: false,
        course: action.payload,
        chapters: action.payload.Chapters
      }

    case GET_COURSE_ERROR:
      return {
        ...state,
        nonExist: true
      }

    case SET_CURRENT_PAGE:
      return {
        ...state,
        isLoading: true
      }

    case SET_CURRENT_PAGE_DONE:
      return {
        ...state,
        isLoading: false,
        currentpage: action.payload
      }

    default:
      return state
  }
}

export const toggle = () => {
  return dispatch => {
    dispatch({
      type: TOGGLE
    })
  }
}

export const getCourse = (id) => {
  return (dispatch, getState) => {
    // only get course if we haven't already got one
    if(getState().appstate.course == null) {
      dispatch({
        type: GET_COURSE
      });

      axios.get(courseUrl+id)
      .then(response => {
        if(response.status === 200 && response.data) {
//          console.log(response.data)
          dispatch({
            type: COURSE_RECEIVED,
            payload: response.data
          });
        } else {
//          console.log('error - no data on course')
          dispatch({
            type: GET_COURSE_ERROR
          });
        }
      })
      .catch((error) => {
//        console.log('error', error)
        dispatch({
          type: GET_COURSE_ERROR
        });
      })
    }
  }
}

export const setCurrentpage = (page) => {
  return (dispatch) => {
    dispatch({
      type: SET_CURRENT_PAGE
    });

    dispatch({
      type: SET_CURRENT_PAGE_DONE,
      payload: page
    });
  }
}