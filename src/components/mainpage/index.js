import React from 'react'
import { connect } from 'react-redux'

import Header from '../header'
import TextArea from '../text'
import Image from '../image'

import './style.scss'

class Article extends React.Component {
	render() {
	  	return (
		  	<div>
				<Image image={this.props.course.Asset} key={Math.random().toString(16).slice(2)} />
		  		<Header content={this.props.course.Title} key={Math.random().toString(16).slice(2)} />
		  		<TextArea content={this.props.course.Description} key={Math.random().toString(16).slice(2)} />
			</div>
		)
	}
}

const mapStateToProps = (state) => ({
	isLoading: state.appstate.isLoading,
	course: state.appstate.course
})

export default connect(
  mapStateToProps,
  null
)(Article)
