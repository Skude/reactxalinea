import React from 'react'
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { toggle, setCurrentpage } from '../../modules/appstate'
import Chapter from '../chapter'
import Mainpage from '../mainpage'

import './style.scss'

const burger = String.fromCharCode(9776);

class ContentArea extends React.Component {
	componentDidMount() {
		this.props.setCurrentpage(this.getCurrentpage(this.props.chapterslug))
	}
	componentDidUpdate(prevProps, prevState) {
		this.props.setCurrentpage(this.getCurrentpage(this.props.chapterslug))
	}

	getCurrentpage() {
		let currentpage
	    for (var i = 0; i < this.props.chapters.length; i++) {
	      if(this.props.chapters[i].Slug === this.props.chapterslug) {
	        currentpage = this.props.chapters[i]
	      }

	      if(i+1 === this.props.chapters.length) {
	      	if(currentpage) {
		        return currentpage
	      	} else {
	      		return null
	      	}
	      }
	    }
	}

	render() {
	  	return (
		  	<div ref='contentarea' className={this.props.menuIsOpen ? "contentarea expanded" : "contentarea"}>
			    <div className="topnav">
					<div className="burger" onClick={this.props.toggle}>{burger}</div>
					<div className="header">
						<div className="left"></div>
					</div>
			    </div>
			    <div className="content">
			    	{this.props.currentpage !== null ? <Chapter /> : <Mainpage />}
			    </div>
			</div>
		)
	}
}

ContentArea.PropTypes = {
	menuIsOpen: PropTypes.bool.isRequired,
	toggle: PropTypes.func.isRequired
}

const mapStateToProps = (state) => ({
  menuIsOpen: state.appstate.menuIsOpen,
  chapters: state.appstate.chapters,
  currentpage: state.appstate.currentpage
})

const mapDispatchToProps = dispatch => bindActionCreators({
  toggle,
  setCurrentpage
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContentArea)