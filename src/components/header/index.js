import React from 'react'

import './style.scss'

class Header extends React.Component {
	render() {
	  	return (
		  	<div>
		  		<h1>{this.props.content}</h1>
			</div>
		)
	}
}

export default Header