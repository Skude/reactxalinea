import React from 'react'

import './style.scss'

class TextArea extends React.Component {
	createMarkup(content) { 
		return {__html: content};
	}

	render() {
//		console.log(this.props.content)
		let parsedText = this.props.content.replace('&nbsp;', '<br /><br />').replace('\n', '<br /><br />')
	  	return (
		  	<div>
		  		<div dangerouslySetInnerHTML={this.createMarkup(parsedText)} />
			</div>
		)
	}
}

export default TextArea