import React from 'react'

import './style.scss'

class Image extends React.Component {

	render() {
	  	return (
		  	<div className='imagearea'>
			  	<img src={this.props.image.BasePath + '/max/1920x1080.jpg'} alt='' key={Math.random().toString(16).slice(2)} />
			</div>
		)
	}
}

export default Image