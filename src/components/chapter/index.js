import React from 'react'
import { connect } from 'react-redux'

import Header from '../header'
import TextArea from '../text'
import Image from '../image'

import './style.scss'

class Chapter extends React.Component {
	render() {
	  	return (
		  	<div>
				  	<Image image={this.props.currentpage.Asset} key={Math.random().toString(16).slice(2)} />
		  			<Header content={this.props.currentpage.Title} key={Math.random().toString(16).slice(2)} />
		  			<TextArea content={this.props.currentpage.IntroDescription} key={Math.random().toString(16).slice(2)} />
			</div>
		)
	}
}

const mapStateToProps = (state) => ({
	isLoading: state.appstate.isLoading,
	currentpage: state.appstate.currentpage
})

export default connect(
  mapStateToProps,
  null
)(Chapter)
