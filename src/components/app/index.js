import React from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getCourse } from '../../modules/appstate'

import MissingCourse from '../missingcourse'
import ContentArea from '../contentarea'
import Nav from '../nav'

import './style.scss'

class App extends React.Component {
  componentDidMount() {
    this.props.getCourse(this.props.match.params.courseslug)
  }
  componentDidUpdate(prevProps, prevState) {
    this.props.getCourse(this.props.match.params.courseslug)
  }
  render() {
    // course fetch returned non-existing course
    if(this.props.nonExist) {
      return (
        <div>
          <main>
            <MissingCourse />
          </main>
        </div>
      )
    } else {
      // Is trying to fetch course data
      if(this.props.isLoading) {
        return (
          <div>
            <main className='loading' />
          </div>
        )
      } else {
        // course fetch returned course data
        return (
          <div className='app'>
            <main>
              <Nav courseslug={this.props.match.params.courseslug} />
              <ContentArea chapterslug={this.props.match.params.chapterslug} />
            </main>
          </div>
        )
      }
    }
  }
}

const mapStateToProps = (state) => ({
  isLoading: state.appstate.isLoading,
  course: state.appstate.course,
  nonExist: state.appstate.nonExist
})

const mapDispatchToProps = dispatch => bindActionCreators({
  getCourse
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)