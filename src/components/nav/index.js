import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { toggle } from '../../modules/appstate'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

import './style.scss'
const close = String.fromCharCode(10005);

class LeftNav extends React.Component {
	render() {
	  	return (
		  <div className={this.props.menuIsOpen ? "nav expanded" : "nav"}>
			    <div className="topnav">
					<div className="burger" onClick={this.props.toggle}>{close}</div>
					<div className="header">
						<div className="right"></div>
					</div>
			    </div>
				<div className='menu'>
				  	<ul>
				  		<Link to={'/course/'+ this.props.courseslug} onClick={this.props.toggle} key={Math.random().toString(16).slice(2)}><li>Forside</li></Link>
						{this.props.menuItems.map(function(menuitem) {
							return <Link to={'/course/'+ this.props.courseslug + '/chapter/' + menuitem.Slug} onClick={this.props.toggle} key={Math.random().toString(16).slice(2)}><li>{menuitem.Title}</li></Link>
						}, this)}
				  	</ul>
				</div>
		  </div>
		)
	}
}

LeftNav.PropTypes = {
	menuIsOpen: PropTypes.bool.isRequired,
	menuItems: PropTypes.array,
	toggle: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  menuIsOpen: state.appstate.menuIsOpen,
  menuItems: state.appstate.chapters
})

const mapDispatchToProps = dispatch => bindActionCreators({
  toggle
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LeftNav)
