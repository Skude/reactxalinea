import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'react-router-redux'
import { Route, Switch, Redirect } from 'react-router-dom'
import store, { history } from './store'

import App from './components/app'
import FrontPage from './components/frontpage'

import './index.scss'

const target = document.querySelector('#root')

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
    	<Switch>
		    <Route exact path="/" component={FrontPage} />
        <Route exact path="/course/:courseslug" component={App} />
		    <Route exact path="/course/:courseslug/chapter/:chapterslug?" component={App} />
			  <Redirect to='/'/>
		</Switch>
    </ConnectedRouter>
  </Provider>,
  target
)
